@extends('layouts.navbar')
@section('content')
    <div class="flex justify-center">
        <div class="col-lg-4 mt-5  align-items-center" style="margin-left: 30%;margin-bottom: 20px">
            <form action="{{route('register.store')}}" method="post"  class="php-email-form">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">Your Name</label>
                        <input type="text" name="name" class="form-control @error('name') border-red-500 @enderror" id="name" placeholder="Your name">
                        @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 mt-3 mt-md-0">
                        <label for="name">Your Email</label>
                        <input type="email" class="form-control  @error('email') border-red-500 @enderror" name="email" id="email" placeholder="Your email">
                        @error('email')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group mt-3">
                    <label for="name">Password</label>
                    <input type="password" class="form-control  @error('password') border-red-500 @enderror" name="password" id="password" placeholder="Your password">
                    @error('password')
                    <div class="text-red-500 mt-2 text-sm">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="form-group mt-3">
                    <label for="password_confirmed" class="sr-only">Password again</label>
                    <input type="password" name="password_confirmation" id="password_confirmed" placeholder="Your password"
                           class="form-control @error('password_confirmation') border-red-500 @enderror"
                           value="">
                    @error('password_confirmation')
                    <div class="text-red-500 mt-2 text-sm">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-success">Send Message</button>
                </div>
            </form>
        </div>
    </div>
@endsection
