@extends('layouts.navbar')
@section('content')

    <div class="flex justify-center">
        @if($posts->count() > 0)
            @foreach($posts as $post)
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <hr>
                        <div class="row text-center">
                            <div class="col-md-4"><h2>{{$post->message}}</h2></div>
                            <div class="col-md-4">@if($post->status ==1)
                                    <button class="btn btn-danger">request</button>
                                @elseif($post->status ==2)
                                    <button class="btn btn-success">confirm</button>
                                @else
                                    <button class="btn btn-dark">returned</button>
                                @endif
                            </div>
                            <div class="col-md-4">{{$post->updated_at}}</div>
                        </div>
                        <hr>
                    </div>
                </div>
            @endforeach
        @else
            <h1 class="text-center text-danger">no request</h1>
        @endif
    </div>



@endsection
