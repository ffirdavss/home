<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::query()->truncate();

        Service::create([
           'icon'=>'bi bi-briefcase',
            'title'=>'Lorem ipsum',
            'body' =>'bla bla bla bla bla bla bla'
        ]);
    }
}
