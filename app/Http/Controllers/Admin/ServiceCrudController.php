<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ServiceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ServiceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        CRUD::setModel(\App\Models\Service::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/service');
        CRUD::setEntityNameStrings('service', 'services');
    }
    protected function setupListOperation()
    {
        CRUD::column('row_number')->type('row_number')->label('#');
        CRUD::column('icon');
        CRUD::column('title');
        CRUD::column('body');
        CRUD::column('created_at');
        CRUD::column('updated_at');

    }
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ServiceRequest::class);

//        CRUD::field('icon');
        CRUD::field('title');
        CRUD::field('body');

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
