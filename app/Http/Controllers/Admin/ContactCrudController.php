<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ContactCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ContactCrudController extends CrudController
{
    use ListOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    public function setup()
    {
        CRUD::setModel(Contact::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/contact');
        CRUD::setEntityNameStrings('contact', 'contacts');
        $this->crud->addFields($this->getFieldsData());
    }

    private function getFieldsData($show = FALSE)
    {
        return [

            [
                'name' => 'subject',
                'label' => 'Subject',
                'type' => 'text'
            ],
            [
                'name' => 'message',
                'label' => 'Message',
                'type' => 'text'
            ],
            [
                'name' => 'status',
                'type' => 'enum',
                'label' => 'Status',
                'value' => [1 => 'request', 2 => 'confirm', 3 => 'returned']
            ],
        ];
    }

    protected function setupListOperation()
    {
//        dd();
        CRUD::column('user_id');
        CRUD::column('subject');
        CRUD::column('message');
        CRUD::column('status');

        $this->crud->addColumns($this->getFieldsData(TRUE));
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(ContactRequest::class);

        CRUD::field('subject');
        CRUD::field('message');
        CRUD::field('status');

    }
}
