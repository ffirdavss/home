<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SliderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;


class SliderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        CRUD::setModel(\App\Models\Slider::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/slider');
        CRUD::setEntityNameStrings('slider', 'sliders');
    }

    protected function setupListOperation()
    {
        CRUD::column('row_number')->type('row_number')->label('#');
        CRUD::column('title');
        CRUD::column('body');
        CRUD::column('image')->type('image');

    }

    protected function setupCreateOperation()
    {

        CRUD::setValidation(SliderRequest::class);
        CRUD::field('title');
        CRUD::field('body');
        CRUD::field('image')->type('image')->crop(true);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
