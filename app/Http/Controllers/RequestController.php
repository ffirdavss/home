<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;

class RequestController extends Controller
{
    public function index()
    {
        $posts = auth()->user()->contact()->with(['user'])->paginate(20);
        return view('request.index', [
            'posts' => $posts,

        ]);
    }
}
