<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Contact;
use App\Models\Image;
use App\Models\Service;
use App\Models\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only(['store']);
    }
    public function index()
    {
        $sliders = Slider::all();
        $services = Service::all();
        $categories = Category::all();
        $images = Image::all()->groupBy('category_id');

        return view('home.index', compact('images', 'categories', 'sliders', 'services'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'message' => 'required',
        ]);
        $request->user()->contact()->create(array_merge(
            $request->only('subject','message'),
            ['status' => 1]
        ));
        return back();
    }
}
